import random
import logging

log = logging.getLogger()
log.setLevel(logging.INFO)

def main():
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    password = ''
    for _ in range(15):
        password += random.choice(chars)
        
    logging.info("Random Password: "+password)

if __name__ == '__main__':
    main()
