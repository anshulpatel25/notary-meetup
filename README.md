# Setup

- Docker >= `17.03`
- Docker-Compose >= `1.21.0`

# Steps

## Spawn the Sandbox

```
docker-compose -f docker-compose.yml up -d
```

## Login into Sandbox

```
docker container exec -it trustsandbox sh
```

## Pull TrustTest Image and tag it for Sandbox Docker Registry

```
cd && mkdir testimage
cd ~/testimage
vi Dockerfile
docker build -f Dockerfile -t myimage:latest .
docker tag myimage:latest sandboxregistry:5000/myimage/myimage:v1
sed -i 's/_1/_2/g' Dockerfile
cat Dockerfile
docker build -f Dockerfile -t myimage:latest .
docker tag myimage:latest sandboxregistry:5000/myimage/myimage:v2
```

## Export Docker Content Trust

```
export DOCKER_CONTENT_TRUST=1
export DOCKER_CONTENT_TRUST_SERVER=https://notaryserver:4443
```

## Push the Image to Docker Registry

```
docker push sandboxregistry:5000/myimage/myimage:v1
docker push sandboxregistry:5000/myimage/myimage:v2
```

## Tamper the Image

```
docker container exec -it sandboxregistry sh

cd /var/lib/registry/docker/registry/v2/blobs/sha256

Swap the Distribution manifest between the images
```

## Only Signed images are visible

```
docker pull alpine
```

## Show the Keys

```
cd 
cd ~/.docker/trust/private

```

## Automation

- Secrets could be automated in CI. Please refer this [link](https://docs.docker.com/engine/security/trust/trust_automation/).

## Destroy Sandbox

```
docker-compose -f docker-compose.yml down
```

## Official Notary

```
notary -s https://notary.docker.io -d ~/.docker/trust list docker.io/library/alpine
```

## Contact

[www.anshulpatel.in](https://anshulpatel.in)