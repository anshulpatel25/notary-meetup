
#!/bin/bash

#Installs and Enables Latest Docker Engine on CentOS Linux
main(){
    sudo yum install -y yum-utils device-mapper-persistent-data lvm2 || exit 1
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo || exit 2
    sudo yum install docker-ce -y  || exit 3
    sudo systemctl enable docker && sudo systemctl start docker || exit 4
    curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py && \
    python /tmp/get-pip.py &&\
    pip install docker-compose
}

main